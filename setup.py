from setuptools import setup
from setuptools import find_packages
import os

thelibFolder = os.path.dirname(os.path.realpath(__file__))
requirementPath = thelibFolder + '/requirements.txt'
install_requires = []
if os.path.isfile(requirementPath):
    with open(requirementPath) as f:
        install_requires = f.read().splitlines()
setup(
    name='TorqueBot',
    version='0.0.1',
    packages=find_packages(),
    url='',
    license='',
    author='David',
    author_email='ruenfei_chong@outlook.com',
    description='Torque Bot to do transfer and reallocation',
    install_requires=install_requires
)
