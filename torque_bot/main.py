import os
import random
import time
import math
from multiprocessing.dummy import Pool as ThreadPool

import xlrd
from pyotp import *
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait

login_url = 'https://torquebot.net/welcome/login'
personal_wallet_url = 'https://torquebot.net/personal-wallet/torq'
accounts_file = 'accounts.xlsx'
chrome_driver = 'chromedriver.exe'
login_failed_accs = []
error_dir = 'error_log'
error_log = '{}/login_failed.txt'.format(error_dir)


def isfloat(x):
    try:
        _a = float(x)
    except (TypeError, ValueError):
        return False
    else:
        return True


def mkdir(path):
    if not os.path.exists(path):
        os.mkdir(path)


def get_accounts():
    wb = xlrd.open_workbook(accounts_file)
    sheet = wb.sheet_by_index(0)
    account_list = []
    allocate_username = ''
    for row in sheet.get_rows():
        if str(row[0].value).lower() != '#' and str(row[1].value).lower() != 'username':
            if int(row[0].value) == 1:
                allocate_username = str(row[1].value)
            account_list.append({
                'id': int(row[0].value),
                'username': str(row[1].value),
                'password': str(row[2].value),
                'secret': str(row[3].value),
                'allocate': allocate_username
            })
    return account_list


def get_otp(secret):
    tfa_otp = TOTP(secret)
    token = tfa_otp.now()
    return token


def login(browser, acc):
    try:
        username = acc.get('username')
        password = acc.get('password')
        secret = acc.get('secret')
        wait = WebDriverWait(browser, 10)

        # Wait until browser able to find username field
        # Key in user name and password field and press ENTER to login
        username_input = wait.until(EC.presence_of_element_located((By.NAME, 'username')))
        time.sleep(random.randint(1, 2))
        username_input.clear()
        split_username = list(username)
        for u in split_username:
            username_input.send_keys(u)
            time.sleep(0.1)
        username_input.send_keys(Keys.TAB)
        password_input = browser.find_element(By.NAME, 'password')
        password_input.clear()
        split_password = list(password)
        for p in split_password:
            password_input.send_keys(p)
            time.sleep(0.1)
        password_input.send_keys(Keys.ENTER)
        # login_btn = browser.find_element_by_id('loginbtn')
        # login_btn.click()

        # Wait until browser able to find first OTP field
        # Generate OTP using given secret key and key it into each OTP field
        time.sleep(random.randint(1, 2))
        invalid_login_txt = browser.find_element(By.ID, 'modalmessage').text
        if invalid_login_txt is not None and (
                str(invalid_login_txt).startswith('Username or Password is incorrect.')
                or str(invalid_login_txt).startswith('Captcha Failed')):
            error_model = browser.find_element(By.ID, 'email-retry-errror')
            ok_btn = error_model.find_element_by_tag_name('button')
            ok_btn.click()
            return str(invalid_login_txt)
        else:
            code_box_1 = wait.until(EC.presence_of_element_located((By.ID, 'codeBox1')))
            code_box_2 = browser.find_element(By.ID, 'codeBox2')
            code_box_3 = browser.find_element(By.ID, 'codeBox3')
            code_box_4 = browser.find_element(By.ID, 'codeBox4')
            code_box_5 = browser.find_element(By.ID, 'codeBox5')
            code_box_6 = browser.find_element(By.ID, 'codeBox6')
            tfa_otp = get_otp(secret)
            split_otp = list(tfa_otp)
            code_box_1.send_keys(split_otp[0])
            time.sleep(0.2)
            code_box_2.send_keys(split_otp[1])
            time.sleep(0.2)
            code_box_3.send_keys(split_otp[2])
            time.sleep(0.2)
            code_box_4.send_keys(split_otp[3])
            time.sleep(0.2)
            code_box_5.send_keys(split_otp[4])
            time.sleep(0.2)
            code_box_6.send_keys(split_otp[5])
            time.sleep(random.randint(1, 2))
            return None
    except WebDriverException as e:
        print('[ERROR] WebDriver Login Message : {}'.format(e.msg))
        now = int(round(time.time() * 1000))
        with open('{}/{}_{}.log'.format(error_dir, 'webdriver_login', now), 'w') as ptf:
            ptf.write('{} - {}'.format(now, e.msg))
        browser.get_screenshot_as_file('{}/{}_{}.png'.format(error_dir, 'login', now))
        return e.msg
    except Exception as e:
        print('[ERROR] Login Message : {}'.format(e.msg))
        now = int(round(time.time() * 1000))
        with open('{}/{}_{}.log'.format(error_dir, 'login', now), 'w') as ptf:
            ptf.write('{} - {}'.format(now, e.msg))
        return e.msg


def perform_allocation(browser, acc):
    try:
        username = acc.get('username')
        secret = acc.get('secret')
        wait = WebDriverWait(browser, 10)

        # Load personal wallet page
        browser.get(personal_wallet_url)
        time.sleep(1)
        # Wait until browser able to find balance field
        # Then click on 'Reallocation' button
        torq_balance_span = wait.until(EC.presence_of_element_located((By.ID, 'currencyName')))
        torq_balance_str = str(torq_balance_span.text).replace('TORQ', '').strip()
        print('>>> TORQ Balance : {}'.format(torq_balance_str))
        if not isfloat(torq_balance_str) or float(torq_balance_str) > 0:
            reallocate_btn = browser.find_element(By.CLASS_NAME, 'reallocate')
            reallocate_btn.click()
            time.sleep(random.randint(1, 2))

            # Wait for dialog open then select reallocation type, reallocation coin
            # Click on MAX button and click next
            reallocate_init = browser.find_element(By.ID, 'reallocate-init')
            reallocation_select = Select(browser.find_element(By.ID, 'reInvest-account'))
            reallocation_select.select_by_value('1')
            # reallocation_select.select_by_visible_text('Re-allocate to current account')
            time.sleep(random.randint(1, 2))
            reallocation_coin_ul = browser.find_element(By.ID, 'reInvest-coin_id')
            reallocation_coin_ul.click()
            reallocation_coin_li = reallocation_coin_ul.find_elements(By.TAG_NAME, 'li')
            for li in reallocation_coin_li:
                li_value = li.get_attribute('value')
                li_text = li.text
                if str(li_value) == '4' and str(li_text).find('Torque to USDT reallocate') > -1:
                    li.click()
                    break
            # reallocation_select.select_by_visible_text('Torque to USDT reallocate')
            max_btn = reallocate_init.find_element(By.TAG_NAME, 'button')
            max_btn.click()
            next_btn = browser.find_element(By.ID, 'TORQ-next')
            next_btn.click()
            time.sleep(1)
            error_content = browser.find_element(By.ID, 'TORQ-error-content')
            if error_content is not None and error_content.text == 'field must contain a number greater than 0.':
                close_btn = browser.find_element(By.CLASS_NAME, 'close')
                close_btn.click()
            else:
                comfirm_otp_input = browser.find_element(By.ID, 'reInvest-auth')
                tfa_otp = get_otp(secret)
                comfirm_otp_input.clear()
                split_tfa = list(tfa_otp)
                for s in split_tfa:
                    comfirm_otp_input.send_keys(s)
                    time.sleep(0.1)
                time.sleep(random.randint(1, 2))
                send_btn = browser.find_element(By.ID, 'TORQ-next')
                send_btn.click()
                time.sleep(1)
                wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'popup-success-icon')))
                ok_btn = browser.find_element(By.ID, 'modalbtn')
                ok_btn.click()
                torq_balance_span = wait.until(EC.presence_of_element_located((By.ID, 'currencyName')))
                print('>>> [{}] TORQ Balance : {}'.format(username, torq_balance_span.text))
        else:
            print('>>> [{}] TORQ Balance not enough to do transfer.'.format(username))
            print('>>> Bot will proceed to logout now.')
        time.sleep(random.randint(0, 2))
        logout_btn = browser.find_element(By.LINK_TEXT, 'Logout')
        logout_btn.click()
        time.sleep(random.randint(1, 2))
    except WebDriverException as e:
        print('[ERROR] WebDriver Perform Transfer Message : {}'.format(e.msg))
        now = int(round(time.time() * 1000))
        with open('{}/{}_{}.log'.format(error_dir, 'webdriver_perform_allocation', now), 'w') as ptf:
            ptf.write('{} - {}'.format(now, e.msg))
        browser.get_screenshot_as_file('{}/{}_{}.png'.format(error_dir, 'perform_allocation', now))
    except Exception as e:
        print('[ERROR] Perform Transfer Message : {}'.format(e.msg))
        now = int(round(time.time() * 1000))
        with open('{}/{}_{}.log'.format(error_dir, 'perform_allocation', now), 'w') as ptf:
            ptf.write('{} - {}'.format(now, e.msg))


def perform_transfer(browser, acc):
    try:
        result = {'success': True, 'msg': None}
        username_to_transfer = acc.get('allocate')
        username = acc.get('username')
        secret = acc.get('secret')
        wait = WebDriverWait(browser, 10)

        # Load personal wallet page
        browser.get(personal_wallet_url)
        time.sleep(random.randint(1, 2))
        # Wait until browser able to find balance field
        # Then click on 'Send to user' button
        torq_balance_span = wait.until(EC.presence_of_element_located((By.ID, 'currencyName')))
        torq_balance_str = str(torq_balance_span.text).replace('TORQ', '').strip()
        print('>>> TORQ Balance : {}'.format(torq_balance_str))
        if not isfloat(torq_balance_str) or float(torq_balance_str) > 0:
            p2p_btn = browser.find_element(By.CLASS_NAME, 'p2p')
            p2p_btn.click()
            time.sleep(random.randint(1, 2))

            # Wait for dialog open then key in recipient username
            # Click on MAX button and click next
            torqtotorq_init = browser.find_element(By.ID, 'torqtotorq-init')
            recipient_input = torqtotorq_init.find_element(By.ID, 'recipient')
            recipient_input.clear()
            recipient_input.send_keys(username_to_transfer)
            time.sleep(0.5)
            max_btn = torqtotorq_init.find_element(By.TAG_NAME, 'button')
            max_btn.click()
            time.sleep(0.5)
            next_btn = browser.find_element(By.ID, 'TORQ-next')
            next_btn.click()
            time.sleep(random.randint(1, 2))
            error_content = browser.find_element(By.ID, 'TORQ-error-content')
            if error_content is not None and error_content.text == 'field must contain a number greater than 0.':
                close_btn = browser.find_element(By.CLASS_NAME, 'close')
                close_btn.click()
            else:
                comfirm_otp_input = browser.find_element(By.NAME, 'torq_torq_transfer_auth')
                tfa_otp = get_otp(secret)
                comfirm_otp_input.clear()
                split_tfa = list(tfa_otp)
                for s in split_tfa:
                    comfirm_otp_input.send_keys(s)
                    time.sleep(0.1)
                time.sleep(random.randint(1, 2))
                send_btn = browser.find_element(By.ID, 'TORQ-next')
                send_btn.click()
                time.sleep(1)
                reinvest_response_modal = wait.until(EC.presence_of_element_located((By.ID, 'reinvest-response-modal')))
                modal_msg = reinvest_response_modal.find_element(By.ID, 'modalmessage').text
                if str(modal_msg).startswith('Authentication code mismatch'):
                    result = {'success': False, 'msg': 'Authentication code mismatch'}
                    ok_btn = browser.find_element(By.ID, 'modalbtn')
                    ok_btn.click()
                    time.sleep(1)
                    close_btn = browser.find_element(By.CLASS_NAME, 'close')
                    close_btn.click()
                else:
                    wait.until(EC.presence_of_element_located((By.CLASS_NAME, 'popup-success-icon')))
                    ok_btn = browser.find_element(By.ID, 'modalbtn')
                    ok_btn.click()
                torq_balance_span = wait.until(EC.presence_of_element_located((By.ID, 'currencyName')))
                print('>>> [{}] TORQ Balance : {}'.format(username, torq_balance_span.text))
        else:
            print('>>> [{}] TORQ Balance not enough to do transfer.'.format(username))
            print('>>> Bot will proceed to logout now.')
        time.sleep(random.randint(0, 2))
        logout_btn = browser.find_element(By.LINK_TEXT, 'Logout')
        logout_btn.click()
        time.sleep(random.randint(1, 2))
        return result
    except WebDriverException as e:
        print('[ERROR] WebDriver Perform Transfer Message : {}'.format(e.msg))
        now = int(round(time.time() * 1000))
        with open('{}/{}_{}.log'.format(error_dir, 'webdriver_perform_transfer', now), 'w') as ptf:
            ptf.write('{} - {}'.format(now, e.msg))
        browser.get_screenshot_as_file('{}/{}_{}.png'.format(error_dir, 'perform_transfer', now))
        return {'success': False, 'msg': e.msg}
    except Exception as e:
        print('[ERROR] Perform Transfer Message : {}'.format(e.msg))
        now = int(round(time.time() * 1000))
        with open('{}/{}_{}.log'.format(error_dir, 'perform_transfer', now), 'w') as ptf:
            ptf.write('{} - {}'.format(now, e.msg))
        return {'success': False, 'msg': e.msg}


def open_browser():
    print('Opening browser...')
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--incognito --start-maximized')
    service = Service(chrome_driver)
    browser = webdriver.Chrome(service=service, options=chrome_options)
    # Load TORQUE login page
    browser.get(login_url)
    return browser


def allocation_bot(account):
    print('Account : {}'.format(account))
    browser = open_browser()
    result = login(browser, account)
    if result is None or result == '':
        perform_allocation(browser, account)
    else:
        login_failed_accs.append({'account': account, 'err': result})
        time.sleep(random.randint(3, 6))
    browser.close()
    browser.quit()


def restart_browser(browser):
    browser.quit()
    time.sleep(random.randint(3, 6))
    browser = open_browser()
    return browser


def perform_bot_transfer(browser, acc):
    print('Account : {}'.format(acc))
    result = login(browser, acc)
    if result is None or result == '':
        transfer_result = perform_transfer(browser, acc)
        return transfer_result
    else:
        login_failed_accs.append({'account': acc, 'err': result})
        if result == 'Captcha Failed':
            return {'success': False, 'msg': 'Captcha Failed'}
        else:
            return {'success': False, 'msg': 'Login Failed'}


def transfer_bot(accs_split):
    if len(accs_split) > 0:
        browser = open_browser()
        for acc in accs_split:
            is_success = False
            retries = 0
            while not is_success and retries < 3:
                result = perform_bot_transfer(browser, acc)
                is_success = result is None
                if result is not None:
                    success = result.get('success')
                    is_success = bool(success)
                    if not success:
                        msg = result.get('msg')
                        if str(msg).startswith('Authentication code mismatch') \
                                or str(msg).startswith('Captcha Failed'):
                            now = int(round(time.time() * 1000))
                            with open('{}/{}_{}.log'.format(error_dir, 'authentication', now), 'w') as ptf:
                                ptf.write('{} - {}'.format(now, str(msg)))
                            browser = restart_browser(browser)
                            retries += 1
                        else:
                            is_success = True
                time.sleep(random.randint(1, 2))
        browser.quit()


if __name__ == '__main__':
    mkdir(error_dir)
    accounts = get_accounts()
    # Get copy first account as reallocation account
    reallocation_acc = accounts[0].copy()
    # Remove reallocation account from account list
    accounts.pop(0)
    thread_nr = 3;
    split_account_nr = math.ceil(len(accounts) / thread_nr)
    if split_account_nr >= 10:
        split_account_nr = 10
    account_split = [accounts[x:x+split_account_nr] for x in range(0, len(accounts), split_account_nr)]

    pool = ThreadPool(3)
    pool.map(transfer_bot, account_split)
    pool.close()
    pool.join()
    print('Transfer complete!!!')
    # allocation_bot(reallocation_acc)
    # print('Allocation complete!!!')
    if len(login_failed_accs) > 0:
        with open(error_log, 'w') as f:
            for lfa in login_failed_accs:
                lfa_acc = lfa.get('account')
                f.write('ID : {} | Username : {} | Error : {}\n'.format(lfa_acc.get('id'), lfa_acc.get('username'),
                                                                        lfa.get('err')))
